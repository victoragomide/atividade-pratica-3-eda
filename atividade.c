#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declaração da Struct Filme
typedef struct tFilme {
  char codigo[8];
  char titulo[100];
  char midia[10];
  char preco[10];
  char genero[20];
} filme;

// Protótipos de Função
FILE *abreArquivoLeituraUsuario();
int contaLinhasArquivo(FILE *arquivo);
filme leArquivoFilmes(FILE *arquivo);
filme *geraArrayFilmes(int linhas, FILE *arquivo);

// Main
int main() {
  int linhas;
  FILE *arquivo;
  filme *filmes;
  arquivo = abreArquivoLeituraUsuario();
  linhas = contaLinhasArquivo(arquivo);
  filmes = geraArrayFilmes(linhas, arquivo);
  fclose(arquivo);
  free(filmes);
  return 0;
}

// Funções
filme *geraArrayFilmes(int linhas, FILE *arquivo) {
  int i;
  filme *filmes, fTemp;
  filmes = (filme*) calloc(linhas,sizeof(filme));
  for(i=0;i<linhas+1;i++) {
    fTemp = leArquivoFilmes(arquivo);
    if(i != 0) { // Ignorando a primeira linha do arquivo (cabeçalho)
      filmes[i-1] = fTemp;
      printf("\nFILME %d\n",i);
      printf("Codigo: %s\nTitulo: %s\nMidia: %s\nPreco: %s\nGenero: %s\n", filmes[i-1].codigo, filmes[i-1].titulo, filmes[i-1].midia, filmes[i-1].preco, filmes[i-1].genero);
    }
  }
  return filmes;
}

filme leArquivoFilmes(FILE *arquivo) {
  filme fTemp;
  if((fscanf(arquivo,"%s %s %s %s %s\n", fTemp.codigo, fTemp.titulo, fTemp.midia, fTemp.preco, fTemp.genero)) != EOF) {
      return fTemp;
  }
}

int contaLinhasArquivo(FILE *arquivo) { // Ignorando a primeira linha (cabeçalho)
  int i=0;
  char aux;
  fseek(arquivo,0,SEEK_SET);
  while((aux=fgetc(arquivo)) != EOF)
		if(aux == '\n')
      i++;
  fseek(arquivo,0,SEEK_SET);
	return i-1;
}

FILE *abreArquivoLeituraUsuario() {
  FILE *arquivo;
  char nome[50];
  char tipo[5];
  strcpy(tipo,".txt");
  printf("Insira o nome do arquivo a ser aberto: ");
  scanf("%50s",nome); // A função scanf() teve de ser usada pois a fgets() automaticamente coloca um \n no fim da string
  strcat(nome,tipo);
  arquivo = fopen(nome,"r");
  if(arquivo == NULL) {
    printf("Erro na abertura do arquivo!\n");
    exit(0);
  }
  printf("Arquivo aberto com sucesso!\n");
  return arquivo;
}
